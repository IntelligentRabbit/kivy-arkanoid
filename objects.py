from random import randint, uniform

from kivy.uix.widget import Widget
from kivy.properties import (
    NumericProperty,
    ReferenceListProperty,
    ColorProperty,
    StringProperty,
    ListProperty,
    BooleanProperty,
)
from kivy.vector import Vector
from kivy.uix.image import Image


class Paddle(Widget):
    w = NumericProperty(0)
    h = NumericProperty(0)
    current_image = NumericProperty(0)
    images = ListProperty()
    image = StringProperty()
    increase = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(Paddle, self).__init__(**kwargs)
        images_pt1 = ["images/paddle/frame-1.png"] * 10
        images_pt2 = ["images/paddle/frame-2.png"] * 10
        images_pt3 = ["images/paddle/frame-3.png"] * 10
        self.images = images_pt1 + images_pt2 + images_pt3

    def bounce_ball(self, ball, sound):
        if self.collide_widget(ball):
            vx, vy = ball.velocity
            offset = (ball.center_x - self.center_x) / (self.width / 2)
            bounced = Vector(vx, -1 * vy)
            vel = bounced * 1.02
            ball.velocity = vel.x + offset, vel.y
            sound.play()

    def change_width_status(self):
        self.increase = not self.increase

    def update(self, dt):
        self.current_image = (self.current_image + 1) % 30
        self.image = self.images[self.current_image]


class Ball(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x, velocity_y)
    color = StringProperty()
    image = StringProperty()
    increase = BooleanProperty(False)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos

    def change_size_status(self):
        self.increase = not self.increase

    def change_image(self):
        if self.color == "red":
            self.image = "images/ball/RedSphere.png"
        if self.color == "orange":
            self.image = "images/ball/OrangeSphere.png"
        if self.color == "yellow":
            self.image = "images/ball/YellowSphere.png"
        if self.color == "green":
            self.image = "images/ball/GreenSphere.png"
        if self.color == "blue":
            self.image = "images/ball/BlueSphere.png"
        if self.color == "purple":
            self.image = "images/ball/PurpleSphere.png"
            
class Brick(Widget):
    x = NumericProperty(0)
    y = NumericProperty(0)
    w = NumericProperty(0)
    h = NumericProperty(0)
    color = ColorProperty()
    broken = BooleanProperty(False)

    def __init__(self, x, y, w, h, color, broken=False):
        super(Brick, self).__init__()
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.color = color
        self.broken = broken

    def bounce_ball_y(self, ball):
        vx, vy = ball.velocity
        offset = (ball.center_x - self.center_x) / (self.width / 2)
        bounced = Vector(vx, -1 * vy)
        vel = bounced * 1.02
        ball.velocity = vel.x + offset, vel.y

    def bounce_ball_x(self, ball):
        vx, vy = ball.velocity
        offset = uniform(-0.5, 0.5)
        bounced = Vector(-1 * vx, vy)
        vel = bounced * 1.02
        ball.velocity = vel.x, vel.y + offset


class Explosion(Image):
    image = StringProperty("")
    images_list = ListProperty()

    def __init__(self, x, y):
        super(Explosion, self).__init__()
        self.x = x
        self.y = y
        for i in range(16):
            self.image = f"images/explosion/{i+1}.png"
            self.images_list.append(self.image)


class Bonus(Image):
    image = StringProperty("")
    side = NumericProperty(0)
    speed = NumericProperty(8)
    type_ = NumericProperty(0)

    def __init__(self, x, y, side):
        super(Bonus, self).__init__()
        self.x = x
        self.y = y
        self.side = side
        self.type_ = randint(1, 4)
        if self.type_ == 1:
            self.image = "images/bonus/extra_attempt.png"
        elif self.type_ == 2:
            self.image = "images/bonus/paddle_increase.png"
        elif self.type_ == 3:
            self.image = "images/bonus/ball_increase.png"
        elif self.type_ == 4:
            self.image = "images/bonus/wall.png"

    def move(self, dt):
        self.y -= self.speed


class Wall(Widget):
    def __init__(self, x, y, width, height):
        super(Wall, self).__init__()
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    def bounce_ball(self, ball, sound):
        if self.collide_widget(ball):
            vx, vy = ball.velocity
            offset = (ball.center_x - self.center_x) / (self.width / 2)
            bounced = Vector(vx, -1 * vy)
            vel = bounced * 1.02
            ball.velocity = vel.x + offset, vel.y
            sound.play()


class Planet(Image):
    pass
    