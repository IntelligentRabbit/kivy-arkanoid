from kivymd.uix.screen import MDScreen
from kivy.animation import Animation

from io_data import save


class SettingsScreen(MDScreen):
    def on_enter(self):
        self.animate_planet_widget()
        
    def on_leave(self):
        self.ids.planet_widget.x = self.width
        
    def sound_btn_press(self):
        self.game.sound_on = not self.game.sound_on
        self.game.sound_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def music_btn_press(self):
        self.game.music_on = not self.game.music_on
        self.game.music_mute_unmute()
        save(
            "saves_settings",
            music_on=self.game.music_on,
            sound_on=self.game.sound_on,
        )

    def return_btn_press(self):
        self.game.current = "welcome_screen"

    
    def animate_planet_widget(self):
        anim = Animation(x=(self.x - self.ids.planet_widget.width), duration=40)
        anim.start(self.ids.planet_widget)
        