from kivy.uix.screenmanager import Screen
from kivy.animation import Animation
from kivy.metrics import sp
from kivy.properties import StringProperty


class WelcomeScreen(Screen):
    image = StringProperty()

    def on_pre_enter(self):
        self.image = "images/welcome.png"

    def on_enter(self):
        self.animate_title_label()
        self.animate_play_btn()
        self.animate_options_btn()

    def on_leave(self):
        self.ids.play_btn.pos_hint = {"center_x": 1, "center_y": 0.68}
        self.ids.options_btn.pos_hint = {"center_x": 1, "center_y": 0.6}

    def animate_title_label(self):
        anim = Animation(font_size=sp(44), duration=0.5)
        anim += Animation(font_size=sp(40), duration=0.5)
        anim.start(self.ids.title_label)

    def animate_play_btn(self):
        anim = Animation(pos_hint={"center_x": 0.7, "center_y": 0.68}, duration=1)
        anim.start(self.ids.play_btn)

    def animate_options_btn(self):
        anim = Animation(pos_hint={"center_x": 0.7, "center_y": 0.6}, duration=1)
        anim.start(self.ids.options_btn)
