from random import choice, uniform
from functools import partial

from kivymd.app import MDApp
from kivymd.uix.button import MDRoundFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.boxlayout import MDBoxLayout
from kivy.core.window import Window
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.properties import (
    NumericProperty,
    BooleanProperty,
    StringProperty,
    ObjectProperty,
    ListProperty,
)
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.core.audio import SoundLoader
from kivy.core.text import LabelBase
from kivy.graphics import Line, Color, Rectangle
from kivy.metrics import dp
from kivy.animation import Animation
from kivy.uix.image import Image
from kivy.utils import get_color_from_hex

from io_data import save, load
from dialog import MyMDDialog
from objects import Ball, Paddle, Brick, Explosion, Bonus, Wall, Planet
from welcome_screen import WelcomeScreen
from settings_screen import SettingsScreen

VERSION = "v. 1.0.4"
SCREEN_DIVIDER = 10
FPS = 60

Builder.load_file("objects.kv")
Builder.load_file("welcome_screen.kv")
Builder.load_file("settings_screen.kv")


class Game(ScreenManager):
    music_on = BooleanProperty(True)
    sound_on = BooleanProperty(True)
    music_loaded = BooleanProperty(False)
    background = StringProperty()
    backgrounds = []
    highscore = NumericProperty(0)
    score = NumericProperty(0)
    player_attempts = NumericProperty(3)
    level = NumericProperty(1)
    pause_btn_disable = BooleanProperty(False)
    txt = StringProperty()
    test = StringProperty()
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for i in range(5):
            bg = f"images/backgrounds/bg-{i+1}.png"
            self.backgrounds.append(bg)

        self.bounce_ball = SoundLoader.load("sounds/bounce-ball.wav")
        self.brick_crash = SoundLoader.load("sounds/crash-glass.wav")
        self.explosion = SoundLoader.load("sounds/explosion.wav")
        self.get_bonus = SoundLoader.load("sounds/bonus.wav")
        self.track_1 = SoundLoader.load("sounds/space-chillout.ogg")
        self.sound_list = [
            self.bounce_ball,
            self.brick_crash,
            self.explosion,
            self.get_bonus,
        ]
        self.music_list = [
            self.track_1,
        ]
        saves_settings = load("saves_settings")
        if len(saves_settings) != 0:
            self.music_on = saves_settings["music_on"]
            self.sound_on = saves_settings["sound_on"]
        self.track_1.loop = True
        self.music_mute_unmute()
        self.track_1.play()
        self.sound_mute_unmute()

        saves_highscore = load("saves_highscore")
        if len(saves_highscore) != 0:
            self.highscore = saves_highscore["highscore"]

    def sound_mute_unmute(self):
        if self.sound_on:
            for sound in self.sound_list:
                sound.volume = 1
        else:
            for sound in self.sound_list:
                sound.volume = 0

    def music_mute_unmute(self):
        if self.music_on:
            for music in self.music_list:
                music.volume = 0.5
        else:
            for music in self.music_list:
                music.volume = 0

    def get_background(self):
        self.background = choice(self.backgrounds)


class GameScreen(Screen):
    game_widget = ObjectProperty(None)
    pause_dialog = None

    def on_pre_enter(self):
        self.game_widget.animate_level_label()

    def get_pause_dialog(self):
        if not self.pause_dialog:
            self.pause_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[36, 36, 36, 36],
                md_bg_color="#696969",
                icon="pause-circle-outline",
                btn_pos="left",
                buttons=[
                    MDRoundFlatButton(
                        text="Continue",
                        text_color=(1, 1, 1, 1),
                        line_color=(1, 1, 1, 1),
                        font_name="fonts/azoft-sans-bold.ttf",
                        on_release=lambda _: self.continue_btn_press(),
                    ),
                    MDRoundFlatButton(
                        text="Restart",
                        text_color=(1, 1, 1, 1),
                        line_color=(1, 1, 1, 1),
                        font_name="fonts/azoft-sans-bold.ttf",
                        on_release=lambda _: self.restart_btn_press(),
                    ),
                ],
            )
        self.pause_dialog.open()

    def press_pause_btn(self):
        for event in self.game_widget.events:
            event.cancel()
        self.get_pause_dialog()

    def restart_btn_press(self):
        self.game_widget.lose = True
        self.game_widget.reset()
        self.pause_dialog.dismiss()
        self.game_widget.animate_level_label()

    def continue_btn_press(self):
        self.pause_dialog.dismiss()
        for event in self.game_widget.events:
            event()


class GameWidget(Widget):
    ball = ObjectProperty(None)
    paddle = ObjectProperty(None)
    wall = ObjectProperty(None)
    size_x = NumericProperty(0)
    size_y = NumericProperty(0)
    brick_w = NumericProperty(0)
    brick_h = NumericProperty(0)
    bricks = ListProperty()
    bonuses = ListProperty()
    bonus_duration = NumericProperty(10)  # in seconds
    lose = BooleanProperty(False)
    gameover_dialog = None
    level_completed_dialog = None
    events = []
    game_widgets = ListProperty()
    colors = {
        "red": "#FF0000",  # red
        "orange": "#FF8C00",  # orange
        "yellow": "#FFD700",  # yellow
        "green": "#50FA05",  # green
        "blue": "#1E90FF",  # blue
        "purple": "#9E64E6",  # purple
    }

    def __init__(self, **kwargs):
        super(GameWidget, self).__init__(**kwargs)
        self.size_x, self.size_y = Window.size
        self.brick_w = self.size_x / SCREEN_DIVIDER - 10
        self.brick_h = self.brick_w / 2

    def start(self):
        self.lose = False
        self.draw_bricks(ArkanoidApp.game.level + 3)
        self.set_paddle_and_ball()

        event = Clock.schedule_interval(self.update, 1 / FPS)
        event2 = Clock.schedule_interval(self.change_color_of_ball, 4)
        self.events = [
            event,
            event2,
        ]

        self.serve_ball()
        ArkanoidApp.game.pause_btn_disable = False

    def update(self, dt):
        self.ball.move()
        self.paddle.bounce_ball(self.ball, ArkanoidApp.game.bounce_ball)
        if not self.paddle.increase:
            self.paddle.update(dt)
        else:
            self.paddle.image = "images/paddle/long.png"
        if self.wall in self.children:
            self.wall.bounce_ball(self.ball, ArkanoidApp.game.bounce_ball)

        for bonus in self.bonuses:
            bonus.move(dt)
            if bonus.y <= -bonus.height:
                self.remove_widget(bonus)
                self.bonuses.remove(bonus)

        if self.ball.x <= self.x or self.ball.x >= self.width:
            self.ball.velocity_x *= -1
            ArkanoidApp.game.bounce_ball.play()

        if self.ball.top >= self.top:
            self.ball.velocity_y *= -1
            self.ball.velocity_x += uniform(-0.5, 0.5)
            ArkanoidApp.game.bounce_ball.play()

        if self.ball.y <= self.y:
            ArkanoidApp.game.player_attempts -= 1
            self.serve_ball()

        self.check_collisions()

        if not self.bricks and not self.lose:
            self.level_completed()

        if ArkanoidApp.game.player_attempts == 0:
            self.lose = True
            self.game_over()

    def on_touch_move(self, touch):
        self.paddle.center_x = touch.x

    def animate_level_label(self):
        ArkanoidApp.game.pause_btn_disable = True
        ArkanoidApp.game.get_background()
        anim = Animation(opacity=1, duration=2) + Animation(opacity=0, duration=2)
        anim.bind(on_complete=lambda a, w: self.start())
        anim.start(self.ids.level_label)

    def set_paddle_and_ball(self):
        self.paddle.w = self.width / 6
        self.paddle.h = self.paddle.w / 4
        self.paddle.center_x = self.center_x
        self.paddle.opacity = 1

        if self.size_x <= 720:
            self.ball.size = [
                24,
            ] * 2
        else:
            self.ball.size = [
                48,
            ] * 2
        ball_colors = list(self.colors.keys())
        self.ball.color = choice(ball_colors)
        self.ball.change_image()
        self.ball.opacity = 1

    def serve_ball(self, vel=(0, -5)):
        self.ball.center = self.center
        self.ball.velocity = vel

    def change_color_of_ball(self, dt):
        ball_colors = list(self.colors.keys())
        self.ball.color = choice(ball_colors)
        self.ball.change_image()

    def draw_bricks(self, number_of_rows):
        brick_colors = list(self.colors.values())
        for i in range(SCREEN_DIVIDER):
            for j in range(number_of_rows):
                brick = Brick(
                    self.x + (self.brick_w + 10) * i,
                    self.height - 100 - (self.brick_h + 10) * (j + 1),
                    self.brick_w,
                    self.brick_h,
                    choice(brick_colors),
                )
                self.add_widget(brick)
                self.bricks.append(brick)

    def bounce_off(self, block):
        if self.ball.velocity_y > self.ball.velocity_x:
            block.bounce_ball_y(self.ball)
        else:
            block.bounce_ball_x(self.ball)

    def remove_bonus_brick(self, target_brick):
        self.remove_widget(target_brick)
        ArkanoidApp.game.explosion.play()
        if target_brick in self.bricks:
            self.bricks.remove(target_brick)
        bonus = Bonus(target_brick.x, target_brick.y, self.brick_w)
        self.add_widget(bonus)
        self.bonuses.append(bonus)

    def get_blast(self, exp_x, exp_y, target_brick):
        exp = Explosion(exp_x, exp_y)
        self.add_widget(exp)

        def change_explosion(img, dt):
            exp.image = exp.images_list[img]

        for i in range(16):
            Clock.schedule_once(partial(change_explosion, i), (0.1 + i * 0.05))

        anim = Animation(w=(target_brick.w - 10), duration=0.2)
        anim &= Animation(h=(target_brick.h - 10), duration=0.2)
        anim &= Animation(pos=(target_brick.x + 10, target_brick.y + 10), duration=0.2)
        anim.bind(on_complete=lambda a, w: self.remove_bonus_brick(target_brick))
        anim.start(target_brick)

    def animate_paddle_increase_bonus(self):
        if not self.paddle.increase:
            self.paddle.change_width_status()
            anim = Animation(width=self.paddle.w * 2, duration=0.2)
            anim &= Animation(opacity=0.95, duration=0.2)
            anim += Animation(opacity=1, duration=self.bonus_duration)
            anim += Animation(width=self.paddle.w, duration=0.2)
            anim.bind(on_complete=lambda a, w: self.paddle.change_width_status())
            anim.start(self.paddle)

    def animate_ball_increase_bonus(self):
        if not self.ball.increase:
            self.ball.change_size_status()
            anim = Animation(
                size=(self.ball.width * 2, self.ball.height * 2), duration=0.2
            )
            anim &= Animation(opacity=0.95, duration=0.2)
            anim += Animation(opacity=1, duration=self.bonus_duration)
            anim += Animation(size=(self.ball.width, self.ball.height), duration=0.2)
            anim.bind(on_complete=lambda a, w: self.ball.change_size_status())
            anim.start(self.ball)

    def animate_wall_bonus(self, widget):
        anim = Animation(opacity=0.95, duration=self.bonus_duration)
        anim.bind(on_complete=lambda a, w: self.remove_widget(widget))
        anim.start(widget)

    def check_collisions(self):
        for brick in self.bricks:
            if brick.collide_widget(self.ball) and not brick.broken:
                brick.broken = True
                self.bounce_off(brick)
                if get_color_from_hex(self.colors[self.ball.color]) == brick.color:
                    self.get_blast(brick.x - 10, brick.y - 10, brick)
                    ArkanoidApp.game.score += 200
                else:
                    ArkanoidApp.game.brick_crash.play()
                    self.remove_widget(brick)
                    self.bricks.remove(brick)
                    ArkanoidApp.game.score += 100
                if ArkanoidApp.game.score > ArkanoidApp.game.highscore:
                    ArkanoidApp.game.highscore = ArkanoidApp.game.score

        for bonus in self.bonuses:
            if bonus.collide_widget(self.paddle):
                ArkanoidApp.game.get_bonus.play()
                if bonus.type_ == 1:
                    ArkanoidApp.game.player_attempts += 1
                elif bonus.type_ == 2:
                    self.animate_paddle_increase_bonus()
                elif bonus.type_ == 3:
                    self.animate_ball_increase_bonus()
                elif bonus.type_ == 4:
                    self.wall = Wall(
                        self.x, self.paddle.y + 100, self.width, self.paddle.h
                    )
                    self.add_widget(self.wall)
                    self.animate_wall_bonus(self.wall)

                self.remove_widget(bonus)
                self.bonuses.remove(bonus)

    def reset(self):
        ArkanoidApp.game.player_attempts = 3
        ArkanoidApp.game.score = 0
        ArkanoidApp.game.level = 1
        self.ball.opacity = 0
        self.paddle.opacity = 0

        for widget in self.children:
            if widget not in [self.ball, self.paddle, self.ids.level_label]:
                self.game_widgets.append(widget)
        self.clear_widgets(children=self.game_widgets)
        self.bricks.clear()

    def get_gameover_dialog(self):
        if not self.gameover_dialog:
            self.gameover_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[36, 36, 36, 36],
                md_bg_color="#696969",
                title="Game Over",
                type="custom",
                content_cls=GameOver(),
                buttons=[
                    MDRoundFlatButton(
                        text="Restart",
                        text_color=(1, 1, 1, 1),
                        line_color=(1, 1, 1, 1),
                        font_name="fonts/azoft-sans-bold.ttf",
                        on_release=lambda _: self.restart_btn_press(),
                    ),
                    MDRoundFlatButton(
                        text="Menu",
                        text_color=(1, 1, 1, 1),
                        line_color=(1, 1, 1, 1),
                        font_name="fonts/azoft-sans-bold.ttf",
                        on_release=lambda _: self.menu_btn_press(),
                    ),
                ],
            )
        self.gameover_dialog.open()

    def restart_btn_press(self):
        self.gameover_dialog.dismiss()
        self.reset()
        self.animate_level_label()

    def menu_btn_press(self):
        self.gameover_dialog.dismiss()
        self.reset()
        ArkanoidApp.game.current = "welcome_screen"

    def game_over(self):
        for event in self.events:
            event.cancel()

        save("saves_highscore", highscore=ArkanoidApp.game.highscore)

        self.get_gameover_dialog()

    def get_level_completed_dialog(self):
        if not self.level_completed_dialog:
            self.level_completed_dialog = MyMDDialog(
                auto_dismiss=False,
                radius=[36, 36, 36, 36],
                md_bg_color="#696969",
                title=f"Level {ArkanoidApp.game.level} completed",
                type="custom",
                content_cls=GameOver(),
                buttons=[
                    MDRoundFlatButton(
                        text="Next",
                        text_color=(1, 1, 1, 1),
                        line_color=(1, 1, 1, 1),
                        font_name="fonts/azoft-sans-bold.ttf",
                        on_release=lambda _: self.next_btn_press(),
                    ),
                ],
            )
        self.level_completed_dialog.open()

    def next_btn_press(self):
        if ArkanoidApp.game.level <= 5:
            ArkanoidApp.game.level += 1
            
            self.animate_level_label()
        else:
            self.reset()
            ArkanoidApp.game.current = "welcome_screen"
        self.level_completed_dialog.dismiss()
        self.level_completed_dialog = None

    def level_completed(self):
        self.ball.opacity = 0
        self.paddle.opacity = 0
        for event in self.events:
            event.cancel()

        save("saves_highscore", highscore=ArkanoidApp.game.highscore)

        for widget in self.children:
            if widget not in [self.ball, self.paddle, self.ids.level_label]:
                self.game_widgets.append(widget)
        self.clear_widgets(children=self.game_widgets)

        self.get_level_completed_dialog()


class TopBar(Widget):
    pass


class GameOver(MDBoxLayout):
    pass


class ArkanoidApp(MDApp):
    version = StringProperty("")

    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.version = VERSION

        ArkanoidApp.game = Game(transition=FadeTransition())
        # self.game.add_widget(LoadingScreen(name="loading_screen"))
        self.game.add_widget(WelcomeScreen(name="welcome_screen"))
        self.game.add_widget(GameScreen(name="game_screen"))
        self.game.add_widget(SettingsScreen(name="settings_screen"))
        return self.game


if __name__ == "__main__":
    LabelBase.register(
        name="Rabbit",
        fn_regular="fonts/azoft-sans.ttf",
        fn_bold="fonts/azoft-sans-bold.ttf",
        fn_italic="fonts/Arka_solid.ttf",
    )
    ArkanoidApp().run()
